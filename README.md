# PlanarMaxCut.jl

Project for solving and approximating max cut on planar graphs

## Installation

Installation is difficult because `LightGraphsMatching` requires `BlossomV`, which doesn't build correctly on Windows and requires `g++` installed on Linux.

### Windows

Just give up.

### Ubuntu 18.04.2 (or WSL Ubuntu)

Install Julia using the official binaries.

Install necessary dependencies:

```sh
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install g++
sudo apt-get install make
```
