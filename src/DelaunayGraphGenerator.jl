#=
 * File: DelaunayGraphGenerator.jl
 * Project: src
 * File Created: Tuesday, 12th November 2019 3:53:37 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 18th November 2019 5:03:52 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module DelaunayGraphGenerator

using LightGraphs, Combinatorics
import VoronoiDelaunay, GeometricalPredicates, PyPlot
using MetricTools, MetricTools.MetricGraphs
using ..WeightedMultigraphs

const VD = VoronoiDelaunay

export gengraph, tessgraph, dualgraph, genallgraphs, randvdpoints, DualMap, GraphDualMapTuple

DualMap = Dict{Tuple{Edge, Float64}, Edge}
GraphDualMapTuple = Tuple{MetricGraph, WeightedMultigraph, DualMap}

"""
Generates random points that can be used by VoronoiDelaunay
"""
randvdpoints(n::Integer)::Array{Point} = 
    Point[(rand(2) .+ VD.min_coord) .* (VD.max_coord - VD.min_coord)
    for i in 1:n]

"""
Generates all possible planar graphs of n vertices
"""
function genallgraphs(points::Array{Point})::Array{GraphDualMapTuple}
    tess::VD.DelaunayTessellation2D = tesselate(points)
    G::MetricGraph = tessgraph(points, tess)
    graphtuples::Array{GraphDualMapTuple} = []
    edges::Array{Edge} = get_edges(G)
    for deledges in combinations(edges)
        G_n::MetricGraph = deepcopy(G)

        for deledge in deledges
            set_weight!(G_n, deledge, 0.)
        end

        G_D::WeightedMultigraph, dualmap::DualMap = dualgraph(G_n, tess)
        push!(graphtuples, (G_n, G_D, dualmap))
    end

    return graphtuples
end

"""
Generates a planar graph and its dual using Delaunay Triangulation
"""
function gengraph(points::Array{Point};
        randweights=false::Bool)::GraphDualMapTuple
    
    tess::VD.DelaunayTessellation2D = tesselate(points)
    G::MetricGraph = tessgraph(points, tess)

    if randweights
        for e in get_edges(G)
            set_weight!(G, e, rand() - 0.5)
        end
    end

    G_D::WeightedMultigraph, dualmap::DualMap = dualgraph(G, tess)
    return G, G_D, dualmap
end

"""
Generates the graph and its dual using Delaunay Triangulation
"""
gengraph(;
        n::Integer=20,
        randweights::Bool=false)::GraphDualMapTuple =
    gengraph(randvdpoints(n);
        randweights=randweights)

"""
Converts a GeometricalPredicates point to a Point
"""
topoint(gppoint)::Point = 
    [GeometricalPredicates.getx(gppoint), GeometricalPredicates.gety(gppoint)]

"""
Tesselates a list of points into a Delaunay Triangulation
"""
function tesselate(points::Array{Point})::VD.DelaunayTessellation2D
    n::Integer = length(points)
    vdpoints = [
        VD.Point(x, y) for (x, y) in points
    ]
    tess = VD.DelaunayTessellation(n)
    push!(tess, vdpoints)
    return tess
end

"""
Creates a MetricGraph out of the Delaunay Triangulation
"""
function tessgraph(
        points::Array{Point},
        tess::VD.DelaunayTessellation2D)::MetricGraph
    G::MetricGraph = MetricGraph(2)
    add!(G, points)

    for edge in VD.delaunayedges(tess)
        a::Point = topoint(VD.geta(edge))
        b::Point = topoint(VD.getb(edge))
        add!(G, (a, b))
    end
    return G
end


"""
Constructs the dual graph to G using the DelaunayTessellation
Returns the Graph, the weights, and the array of perimeter vertices
"""
function dualgraph(
        G::MetricGraph,
        tess::VD.DelaunayTessellation2D
        )::Tuple{WeightedMultigraph, Dict{Tuple{Edge, Float64}, Edge}}

    G_D::WeightedMultigraph = WeightedMultigraph(1)
    triangleids::Dict{VD.DelaunayTriangle, Integer} = 
        Dict{VD.DelaunayTriangle, Integer}()
    dualmap::Dict{Tuple{Edge, Float64}, Edge} = Dict{Tuple{Edge, Float64}, Edge}()
        
    # add all triangles and assign them ids
    for triangle in tess
        if !(triangle in keys(triangleids))
            add_vertex!(G_D)
            triangleids[triangle] = nv(G_D)
        end
    end
    
    for triangle in tess
        addtriangle!(G_D, G, tess, triangle, triangleids, dualmap)
    end
    
    return G_D, dualmap
end

"""
Adds the appropriate edges of a triangle to the dual graph
Returns the number of perimeter edges
"""
function addtriangle!(
        G_D::WeightedMultigraph,
        G::MetricGraph,
        tess::VD.DelaunayTessellation2D,
        triangle::VD.DelaunayTriangle,
        triangleids::Dict{VD.DelaunayTriangle, Integer},
        dualmap::Dict{Tuple{Edge, Float64}, Edge})
    tid::Integer = triangleids[triangle]

    a = topoint(VD.geta(triangle))
    b = topoint(VD.getb(triangle))
    c = topoint(VD.getc(triangle))

    neighbors::Array{VD.DelaunayTriangle} = [
        VD.movea(tess, triangle),
        VD.moveb(tess, triangle),
        VD.movec(tess, triangle)
    ]
    weights::Array{Float64} = [
        distance(G, b, c),
        distance(G, a, c),
        distance(G, a, b)
    ]
    edges::Array{Edge} = [
        Edge(get_vertex(G, b), get_vertex(G, c)),
        Edge(get_vertex(G, a), get_vertex(G, c)),
        Edge(get_vertex(G, a), get_vertex(G, b))
    ]
    
    for i in eachindex(neighbors)
        if neighbors[i] in keys(triangleids)
            nid::Integer = triangleids[neighbors[i]]
            if has_edge(G_D, tid, nid) continue end # don't double add edges
            add_edge!(G_D, tid, nid, weights[i])
            dualmap[(edge(tid, nid), weights[i])] = edges[i]
        else
            # only the edges for the perimeter can have multiple
            add_edge!(G_D, tid, 1, weights[i])
            dualmap[(edge(tid, 1), weights[i])] = edges[i]
        end
    end
end

end
