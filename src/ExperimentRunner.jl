#=
 * File: ExperimentRunner.jl
 * Project: src
 * File Created: Tuesday, 12th November 2019 9:33:43 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 18th November 2019 5:18:38 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module ExperimentRunner

using Dates, LightGraphs, JSON2
import PyPlot
using MetricTools, MetricTools.MetricGraphs

using
    ..WeightedMultigraphs,
    ..OptMaxCut,
    ..LocalSearch,
    ..DelaunayGraphGenerator

export ExperimentResult, AlgResult, save, load, 
    runexp, runrandexp, rundelexps, graphresults, cutplot, barplot

"""
Saves the results of running an algorithm
"""
struct AlgResult
    val::Float64
    time::Float64
end

"""
Struct for saving experiments
"""
struct ExperimentResult
    points::Array{Point}
    algs::Dict{String, AlgResult}    # maps lookahead to alg
end

#region files
"""
Saves an experiment
"""
function save(result::ExperimentResult; dir="data/output/results/"::String)
    mkpath(dir)
    filename::String = Dates.format(now(), "mm-dd-HH-MM-s")
    open("$dir$filename.json", "w") do io
        write(io, JSON2.write(result))
    end
end

"""
Loads experiment result data from the directory
"""
function load(; dir="data/output/results/"::String)::Array{ExperimentResult}
    results::Array{ExperimentResult} = []
    resultstr::String = ""
    
    for (root, dirs, files) in walkdir(dir)
        for file in files
            println("    ", joinpath(root, file)) # path to Files
            open(joinpath(dir, file), "r") do io
                resultstr = read(io, String)
            end
            push!(results, JSON2.read(resultstr, ExperimentResult))
        end
        break
    end
    return results
end

"""
Loads all experiments 
"""
function loadall(dir::String)
    results::Array{ExperimentResult} = []
    for (root, dirs, files) in walkdir(dir)
        for dir in dirs
            println(joinpath(root, dir)) # path to directories
            results = [results; load(; dir=joinpath(root, dir))]
        end
        break
    end
end
#endregion files

#region plots
"""
Creates a bar plot using PyPlot
"""
function barplot(labels::Array{String}, values::Array{Float64};
        filename="bar"::String)
    PyPlot.close("all")
    index = [i-1 for i in eachindex(labels)]
    PyPlot.bar(index, values, align="center", alpha=0.5)
    PyPlot.xticks(index, labels)
    PyPlot.ylabel("")
    PyPlot.title("")
    ax = PyPlot.gca()
    for (i, v) in enumerate(values)
        ax.text(v + 3, i + .25, "$v")
    end

    dir::String = "data/output/images/"
    mkpath(dir)
    PyPlot.tight_layout()
    PyPlot.savefig("$dir$filename.png")
end

"""
Plots a cut on a graph
"""
function cutplot(G::MetricGraph, edges::Array{Edge};
        dir="data/output/images/"::String, name="cut"::String)
    mkpath(dir)
    
    PyPlot.close("all")
    PyPlot.rc("font", family="serif", size=18)
    plot(G)

    PyPlot.tight_layout()
    PyPlot.savefig("$dir$name-G.png")

    lines::Array{Line} = [line for line in get_lines(G, edges)
        if distance(G, line[1], line[2]) != 0]
    plot(lines; color="red")
    
    PyPlot.tight_layout()
    PyPlot.savefig("$dir$name.png")
end

"""
Graphs the percent error of all results
"""
function graphresults(results::Array{ExperimentResult}; filename="vals")
    n::Integer = length(results)
    totalvs::Dict{String, Float64} = Dict{String, Float64}()
    totalts::Dict{String, Float64} = Dict{String, Float64}()

    for result in results
        for (name, alg) in result.algs
            if !(name in keys(totalvs)) totalvs[name] = 0 end
            if !(name in keys(totalts)) totalts[name] = 0 end

            totalvs[name] += alg.val
            totalts[name] += alg.time
        end
    end

    names::Array{String} = collect(keys(totalvs))
    barplot(names, [totalvs[name] / n for name in names]; filename=filename)
    barplot(names, [totalts[name] / n for name in names]; filename="$filename-t")
    for name in names
        println(name)
        println("    Average val: ", totalvs[name] / n)
        println("    Average time: ", totalts[name] / n)
    end
end
#endregion plots

#region experiments
"""
Runs the deletion experiments, constructing all planar graphs
"""
function rundelexps(points::Array{Point};
        lookaheads=Integer[1, 2]::Array{Integer}, verbose=false::Bool)
    println("Testing deletion experiments...")
    allgraphs::Array{GraphDualMapTuple} = genallgraphs(points)
    for (i, (G, G_D, dualmap)) in enumerate(allgraphs)
        runexp(G, G_D, dualmap;
            lookaheads=lookaheads, verbose=verbose, cutplotdir="data/output/images/dels/$i/")
    end
end

"""
Tests Optimal Max Cut on a random case
"""
function runrandexp(;
        n=10::Integer, verbose=false::Bool, randweights=false::Bool,
        lookaheads=Integer[1, 2]::Array{Integer})    
    println("Testing random experiment...")
    G::MetricGraph, G_D::WeightedMultigraph, dualmap::DualMap =
        gengraph(; n=n, randweights=randweights)
    
    runexp(G, G_D, dualmap; lookaheads=lookaheads, verbose=verbose, randweights=randweights)
end

"""
Runs an experiment
Compares performance of OPTP to LOCALSEARCH-N for a given graph and its pre-computed dual
"""
function runexp(G::MetricGraph, G_D::WeightedMultigraph, dualmap::DualMap;
        lookaheads=Integer[1, 2]::Array{Integer},
        randweights=false::Bool,
        verbose=false::Bool,
        cutplotdir="data/output/images/"::String)

    algs::Dict{String, AlgResult} = Dict{String, AlgResult}()
    OPT::String = "OPT"
    allperfect::Bool = true

    (optcut, optval), t, bytes, gctime, memallocs =
        @timed optmaxcut(G, G_D, dualmap; verbose=false)
    algs[OPT] = AlgResult(optval, t)
    println("    $OPT = $optval in $t sec")
    
    for lookahead in lookaheads
        
        ALG::String = "ALG$lookahead"
        (algcut, algval, iters), t, bytes, gctime, memallocs = 
            @timed localsearch(G; lookahead=lookahead, verbose=verbose)
        algs[ALG] = AlgResult(algval, t)
        println("    $ALG = $algval in $t secs")

        # if the approximation wasn't perfect, plot the example
        if !(algval ≈ optval)
            if verbose cutplot(G, cut2edges(G, algcut); dir=cutplotdir, name=ALG) end
            allperfect = false
        end
    end
    
    # if the approximations weren't all perfect, don't plot
    if verbose && !allperfect cutplot(G, collect(optcut); dir=cutplotdir, name=OPT) end
    
    result::ExperimentResult = ExperimentResult(
        get_points(G), algs)

    dir::String = "data/output/results/"
    dirname::String = join(keys(result.algs), "-") * (randweights ? "-RW" : "")
    n::Integer = length(result.points)
    dir *= "$dirname/$n/"
    save(result; dir=dir)
end
#endregion experiments

end
