#=
 * File: LocalSearch.jl
 * Project: src
 * File Created: Wednesday, 23rd October 2019 2:35:26 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 17th November 2019 11:18:41 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module LocalSearch
using LightGraphs
using Combinatorics
using MetricTools, MetricTools.MetricGraphs

export localsearch, evalcut, cut2edges

"""
Performs local search with n lookahead steps
"""
function localsearch(G::MetricGraph;
    initial=Set{Integer}()::Set{Integer},
    lookahead=1::Integer,
    verbose=false::Bool)::Tuple{Set{Integer}, Float64, Integer}

    cut::Set{Integer} = initial    
    maxcut::Set{Integer} = cut
    val::Float64 = evalcut(G, cut)
    maxval::Float64 = val
    prevmax::Float64 = maxval
    vertices::Array{Integer} = get_vertices(G)
    i::Integer = 0

    if verbose println("Running local search with initial cut $cut...") end

    while true
        # try all combinations of size 1, then size 2, then size 3, up through lookahead
        for n in 1:lookahead
            for flippers in combinations(vertices, n)
                # make action
                flip(cut, flippers)
                            
                # check value
                val = evalcut(G, cut)
                if val > maxval
                    maxval = val
                    maxcut = copy(cut)
                    break
                end

                # undo the action
                flip(cut, flippers)
            end
        end
        if maxval == prevmax break end
        if verbose println("    val = $maxval, cut = $maxcut") end

        prevmax = maxval
        cut = maxcut
        i += 1
    end

    if verbose println("    maxval = $maxval, maxcut = $(sort(collect(maxcut)))") end
    return maxcut, maxval, i
end

"""
Converts a cut to a list of edges
"""
function cut2edges(G::MetricGraph, cut::Set{Integer})
    edges::Array{Edge} = []
    for e in get_edges(G)
        if (e.src in cut) != (e.dst in cut)
            push!(edges, e)
        end
    end
    return edges
end

"""
Flips membership of all vertices in the set
"""
function flip(cut::Set{<:Integer}, flippers::Array{<:Integer})
    for flipper in flippers
        if flipper in cut
            pop!(cut, flipper)
        else
            push!(cut, flipper)
        end
    end
end

"""
Evalutates the weight of a cut
"""
function evalcut(G::MetricGraph, cut::Set{Integer})::Float64
    val::Float64 = 0
    for e in get_edges(G)
        if (e.src in cut) != (e.dst in cut)
            val += distance(G, e)
        end
    end
    return val
end

"""
Evalutates the weight of a cut
"""
function evalcut(G::MetricGraph, cut::Set{Edge})::Float64
    val::Float64 = 0
    for e in cut
        val += distance(G, e)
    end
    return val
end

end
