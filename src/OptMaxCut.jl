#=
 * File: OptMaxCut.jl
 * Project: src
 * File Created: Wednesday, 23rd October 2019 2:36:16 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Saturday, 16th November 2019 4:46:47 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module OptMaxCut
using MetricTools, MetricTools.MetricGraphs
using LightGraphs
using ..PerfectMatching, ..WeightedMultigraphs
using ..LocalSearch: evalcut

export optmaxcut

"""
Computes the optimal max cut given a dual graph in polynomial time
"""
function optmaxcut(G::MetricGraph, G_D::WeightedMultigraph, dualmap::Dict{Tuple{Edge, Float64}, Edge};
        verbose=false::Bool)::Tuple{Set{Edge}, Float64}
    if verbose printdetails(G_D, "G_D") end
    
    # Create G_t, a graph with max degree 4 by splitting all high-degree vertices
    G_t::WeightedMultigraph, splitmap::Dict{Integer, Integer} = split(G_D; verbose=verbose)
    if verbose
        printdetails(G_t, "G_t")
        println("splitmap: $splitmap\n")
    end

    # Create G_E, an expanded graph where each vertex is replaced by a K4 Kasteleyn City
    G_E::WeightedMultigraph, expandmap::Dict{Integer, Integer} = expand(G_t; verbose=verbose)
    if verbose
        printdetails(G_E, "G_E")
        println("expandmap: $expandmap\n")
    end

    # Compute the max
    match::MatchingResult = maxwpm(G_E.g, firstweights(G_E))
    
    cut::Set{Edge} = extractcut(G_D, match, splitmap, expandmap, dualmap)
    if verbose
        println("match.weight = $(match.weight)")
        println("evalcut(cut) = $(evalcut(G, cut))")
    end
    return cut, match.weight
end

"""
Extracts the cut from the results of the perfect matching
This seems hard to do... just returning edges for now
TODO
"""
function extractcut(
        G_D::WeightedMultigraph,
        match::MatchingResult,
        splitmap::Dict{Integer, Integer},
        expandmap::Dict{Integer, Integer},
        dualmap::Dict{Tuple{Edge, Float64}, Edge};
        verbose=false::Bool)::Set{Edge}

    edges::Set{Edge} = Set{Edge}()
    
    """
    uses the maps to get the original vertex
    """
    function original(v::Integer)::Integer
        vG_t::Integer = v in keys(expandmap) ? expandmap[v] : v
        vG_D::Integer = vG_t in keys(splitmap) ? splitmap[vG_t] : vG_t
        return vG_D
    end
    
    if verbose
        println("dualmap: $dualmap")
        println("mate: $(match.mate)")
    end

    for (v1, v2) in enumerate(match.mate)
        if v1 > v2 continue end
        
        w::Float64 = -match.weights[Edge(v1, v2)]
        if w == 0 continue end
        
        o1::Integer, o2::Integer = original(v1), original(v2)
        if o1 == o2 continue end

        dualedge::Edge = edge(o1, o2)
        if (dualedge, w) in keys(dualmap) push!(edges, dualmap[(dualedge, w)])
        else println("    NOT IN MAP: $((dualedge, w))") end
    end

    return edges
end

"""
Replaces all vertices with degree d > 4 with (d-1)/2 representative vertices
With the edges distributed amongst them
"""
function split(G_D::WeightedMultigraph;
        verbose=false::Bool)::Tuple{WeightedMultigraph, Dict{Integer, Integer}}
    G_t::WeightedMultigraph = deepcopy(G_D)
    splitmap::Dict{Integer, Integer} = Dict{Integer, Integer}()
    
    for v in vertices(G_D)
        d::Integer = WeightedMultigraphs.degree(G_D, v)
        if d <= 4 continue end
        split!(G_t, splitmap, v, d)
    end

    return G_t, splitmap
end

"""
Splits vertex v into (d-1)/2 representative vertices
"""
function split!(
        G_t::WeightedMultigraph,
        splitmap::Dict{Integer, Integer},
        v::Integer,
        d::Integer)
    
    totalsplits::Integer = floor(Int64, (d - 1)/2)
    
    # add all new representative vertices (use the same v for first)
    newvs::Array{Integer} = [v]
    for split in 1:totalsplits
        add_vertex!(G_t)
        newv::Integer = nv(G_t)
        push!(newvs, newv)
        splitmap[newv] = v
    end

    neighbweights::Array{Tuple{Integer, Float64}} = weights(G_t, v)
    rem_all_edges!(G_t, v)

    # create connections between all representatives
    for i in 2:lastindex(newvs)
        add_edge!(G_t, newvs[i - 1], newvs[i], 0.)
    end
    
    # distribute the edges to the new vertices via round robin
    for (i, (nv, nw)) in enumerate(neighbweights)
        add_edge!(G_t, nv, newvs[(i % length(newvs)) + 1], nw)
    end
end

"""
Expands a multigraph into K4s
Assumes that the degree of every node in the given multigraph is 3 or 4
"""
function expand(G_t::WeightedMultigraph;
        verbose=false::Bool)::Tuple{WeightedMultigraph, Dict{Integer, Integer}}
    G_E::WeightedMultigraph = deepcopy(G_t)
    expandmap::Dict{Integer, Integer} = Dict{Integer, Integer}()

    for v in vertices(G_t)
        expand!(G_E, expandmap, v)
    end
    return G_E, expandmap
end

"""
Expands a vertex v into a kcity
"""
function expand!(G_E::WeightedMultigraph, expandmap::Dict{Integer, Integer}, v::Integer)
    
    # add all new representative vertices (use the same v for first)
    newvs::Array{Integer} = [v]
    neighbweights::Array{Tuple{Integer, Float64}} = weights(G_E, v)

    rem_all_edges!(G_E, v)

    for split in 1:3
        add_vertex!(G_E)
        newv::Integer = nv(G_E)
        push!(newvs, newv)
        expandmap[newv] = v
    end

    # create a clique between all pairs
    for (newv1, newv2) in Iterators.product(newvs, newvs)
        if newv1 == newv2 || has_edge(G_E, newv1, newv2) continue end
        add_edge!(G_E, newv1, newv2, 0.)
    end

    for (i, (nv, nw)) in enumerate(neighbweights)
        add_edge!(G_E, newvs[i], nv, nw)
    end
end

end
