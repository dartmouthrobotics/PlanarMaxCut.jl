#=
 * File: PerfectMatching.jl
 * Project: src
 * File Created: Wednesday, 23rd October 2019 2:43:32 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Thursday, 14th November 2019 11:05:19 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module PerfectMatching

using LightGraphs
import BlossomV

export minwpm, maxwpm, MatchingResult

"""
Matching result
"""
mutable struct MatchingResult{U<:Real}
    weight::U
    mate::Array{Int}
    weights::Dict{Edge, U}
end

"""
Code extracted and modified from the LigthGraphsMatching.jl package

    minwpm(g, w::Dict{Edge,Real})
    minwpm(g, w::Dict{Edge,Real}, cutoff)

Given a graph `g` and an edgemap `w` containing weights associated to edges,
returns a matching with the mimimum total weight among the ones containing
exactly `nv(g)/2` edges.

Edges in `g` not present in `w` will not be considered for the matching.

This function relies on the BlossomV.jl package, a julia wrapper
around Kolmogorov's BlossomV algorithm.

Eventually a `cutoff` argument can be given, to the reduce computational time
excluding edges with weights higher than the cutoff.

The returned object is of type `MatchingResult`.

In case of error try to change the optional argument `tmaxscale` (default is `tmaxscale=10`).
"""
function minwpm end
function maxwpm end

"""
Max weight perfect matching
Internally works by inverting weights and calling min weight perfect matching
"""
function maxwpm(g::Graph, w::Dict{E,U}; tmaxscale=10.) where {U<:Real, E<:Edge}
    # make all weights negative
    wnew::Dict{E,U} = Dict{E,U}(e => -v for (e, v) in w)
    match = minwpm(g, wnew; tmaxscale=tmaxscale)
    match.weight = -match.weight
    return match
end

"""
Min weight perfect matching with a cutoff
"""
function minwpm(g::Graph, w::Dict{E,U}, cutoff, kws...) where {U<:Real, E<:Edge}
    wnew = Dict{E, U}()
    for (e, c) in w
        if c <= cutoff
            wnew[e] = c
        end
    end
    return minwpm(g, wnew; kws...)
end

"""
Performs min weight perfect matching on a general graph with |V| mod 2 = 0
Internally converts to unsigned integers with a scaling procedure
"""
function minwpm(g::Graph, w::Dict{E,U};
        tmaxscale=10., verbose=false::Bool) where {U<:AbstractFloat, E<:Edge}
    wnew = Dict{E, Int32}()
    cmax = maximum(values(w))
    cmin = minimum(values(w))
    tmax = typemax(Int32)  / tmaxscale # /10 is kinda arbitrary,
                                # hopefully high enough to not occur in overflow problems
    if verbose println("    minwpm weight scaling: cmax = $cmax, cmin = $cmin") end
    if cmax-cmin != 0
        for (e, c) in w
            wnew[e] = round(Int32, (c-cmin) / (cmax-cmin) * tmax)
        end
    else
        # handles case of all equal weighted negative edges
        for (e, c) in w
            wnew[e] = abs(round(Int32, c))
        end
    end
    
    match = minwpmint(g, wnew)
    weight = zero(U)
    for i=1:nv(g)
        j = match.mate[i]
        if j > i
            weight += w[E(i,j)]
        end
    end
    return MatchingResult(weight, match.mate, w)
end

"""
Performs min weight perfect mathcing on an unsigned integer weighted graph
"""
function minwpmint(g::Graph, w::Dict{E,U}) where {U<:Integer, E<:Edge}
    m = BlossomV.Matching(nv(g), ne(g))
    for (e, c) in w
        BlossomV.add_edge(m, src(e)-1, dst(e)-1, c)
    end
    BlossomV.solve(m)

    mate = fill(-1, nv(g))
    totweight = zero(U)
    for i=1:nv(g)
        j = BlossomV.get_match(m, i-1) + 1
        mate[i] = j <= 0 ? -1 : j
        if i < j
            totweight += w[Edge(i,j)]
        end
    end
    return MatchingResult(totweight, mate, w)
end

end
