#=
 * File: PlanarGraphGenerator.jl
 * Project: src
 * File Created: Wednesday, 23rd October 2019 2:34:22 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 18th November 2019 5:29:00 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module PlanarGraphGenerator

import VoronoiDelaunay
using MetricTools, MetricTools.MetricGraphs, MetricTools.Geometry
using ..PlanarMaxCut

const VD = VoronoiDelaunay

export gengraphg

"""
Generates random points that can be used by VoronoiDelaunay
"""
randvdpoints(n::Integer)::Array{Point} = 
    Point[(rand(2) .+ VD.min_coord) .* (VD.max_coord - VD.min_coord)
    for i in 1:n]

"""
Adds an edge if it doesn't overlap with previous edges
"""
function addiffree!(G::MetricGraph, line::Line)
    for otherline in get_lines(G)
        if intersects(otherline, line; onedge=false) return end
    end
    add!(G, line)
end

"""
Generates a random graph 
"""
function gengraphg(n::Integer)::MetricGraph
    dim::Integer = 2
    points::Array{Point} = randvdpoints(n)
    G::MetricGraph = MetricGraph(dim)
    add!(G, points)

    for (p1, p2) in Iterators.product(points, points)
        if p1 == p2 continue end
        addiffree!(G, (p1, p2))
    end
    return G
end

end
