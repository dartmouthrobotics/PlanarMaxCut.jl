#=
 * File: PlanarMaxCut.jl
 * Project: src
 * File Created: Wednesday, 23rd October 2019 2:25:57 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 18th November 2019 5:23:53 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module PlanarMaxCut
using MetricTools

include("./WeightedMultigraphs.jl")
include("./LocalSearch.jl")
include("./PerfectMatching.jl")
include("./DelaunayGraphGenerator.jl")
include("./PlanarGraphGenerator.jl")
include("./OptMaxCut.jl")
include("./ExperimentRunner.jl")

using
    .WeightedMultigraphs,
    .LocalSearch,
    .OptMaxCut,
    .ExperimentRunner,
    .DelaunayGraphGenerator

export 
    localsearch,
    optmaxcut,
    gengraph,
    gengraphg,
    runexp,
    runrandexp,
    rundelexps,
    WeightedMultigraph,
    DualMap,
    ExperimentResult,
    AlgResult,
    load,
    save,
    graphresults,
    cutplot,
    barplot,
    cut2edges
    
end # module
