#=
 * File: WeightedMultigraphs.jl
 * Project: src
 * File Created: Saturday, 9th November 2019 5:50:12 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Thursday, 14th November 2019 10:36:32 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#
module WeightedMultigraphs

using MetricTools
using LightGraphs

export
    WeightedMultigraph, degree, rem_all_edges!,
    printdetails, firstweights, weight, edge

"""
Struct for a simple weighted multigraph
Implemented via a hashtable of edges to lists of weights
"""
struct WeightedMultigraph
    g::Graph
    w::Dict{Edge, Array{Float64}}
    WeightedMultigraph() = new(SimpleGraph(), Dict{Edge, Array{Float64}}())
    WeightedMultigraph(n::Integer) = new(SimpleGraph(n), Dict{Edge, Array{Float64}}())
end

LightGraphs.add_vertex!(wmg::WeightedMultigraph) = add_vertex!(wmg.g)
LightGraphs.nv(wmg::WeightedMultigraph) = nv(wmg.g)
LightGraphs.ne(wmg) = ne(wmg.g) == 0 ? 0 : sum(length(ws) for ws in values(wmg.w))
LightGraphs.vertices(wmg::WeightedMultigraph) = vertices(wmg.g)
LightGraphs.outneighbors(wmg::WeightedMultigraph, v::Integer) = outneighbors(wmg.g, v)
LightGraphs.has_edge(wmg::WeightedMultigraph, v1::Integer, v2::Integer)::Bool =
    has_edge(wmg.g, v1, v2)

"""
Removes an edge from the graph, popping off the first added weight
"""
function LightGraphs.rem_edge!(wmg::WeightedMultigraph, v1::Integer, v2::Integer)
    if !has_edge(wmg, v1, v2) return end
    e::Edge = edge(v1, v2)
    wmg.w[e].pop!()
    if isempty(wmg.w[e]) rem_edge(wmg.g, e) end
end

"""
Removes all edges for a vertex
"""
function rem_all_edges!(wmg::WeightedMultigraph, v::Integer)
    neighbors::Array{Integer} = outneighbors(wmg, v)
    for nv in neighbors
        rem_all_edges!(wmg, v, nv)
    end
end

"""
Removes all edges between two vertices
"""
function rem_all_edges!(wmg::WeightedMultigraph, v1::Integer, v2::Integer)
    if !has_edge(wmg, v1, v2) return end
    e::Edge = edge(v1, v2)
    delete!(wmg.w, e)
    rem_edge!(wmg.g, e)
end

"""
Creates an edge from 2 vertices with the lowest v first
"""
edge(v1::Integer, v2::Integer)::Edge = v2 < v1 ? Edge(v2, v1) : Edge(v1, v2)

"""
Gets all weights for this edge
"""
LightGraphs.weights(wmg::WeightedMultigraph, v1::Integer, v2::Integer)::Array{Float64} =
    wmg.w[edge(v1, v2)]

"""
Gets all weights to all neighbors from this edge as [(nv1, w1), (nv1, w2), ...]
"""
function LightGraphs.weights(wmg::WeightedMultigraph, v::Integer)::Array{Tuple{Integer, Float64}}
    neighbweights::Array{Tuple{Integer, Float64}} = []
    for nv in outneighbors(wmg, v)
        for w in weights(wmg, v, nv)
            neighbweights = push!(neighbweights, (nv, w))
        end
    end
    return neighbweights
end

"""
Gets the first weight
"""
function weight(wmg::WeightedMultigraph, v1::Integer, v2::Integer)::Float64
    if v1 > v2 v1, v2 = v2, v1 end
    first(weights(wmg, v1, v2))
end
    
"""
Adds an edge with a given weight
"""
function LightGraphs.add_edge!(wmg::WeightedMultigraph, v1::Integer, v2::Integer, w::Float64)
    e::Edge = edge(v1, v2)
    
    add_edge!(wmg.g, e)
    if !(e in keys(wmg.w)) wmg.w[e] = [] end
    push!(wmg.w[e], w)
end

"""
Returns the degree of a vertex, assuming undirected (|outneighbors| = |inneighbors|)
"""
function LightGraphs.degree(wmg::WeightedMultigraph, v::Integer)::Integer
    total::Integer = 0
    for neighbor in outneighbors(wmg.g, v)
        neighboredge = edge(v, neighbor)
        if !(neighboredge in keys(wmg.w)) continue end
        total += length(wmg.w[neighboredge])
    end
    return total
end

"""
Returns the first weights for all of the edges of the multigraph
"""
firstweights(wmg::WeightedMultigraph)::Dict{Edge, Float64} = 
    Dict{Edge, Float64}(e => first(ws) for (e, ws) in wmg.w)
    
"""
Prints the details of the graph
"""
function printdetails(wmg::WeightedMultigraph, name::String)
    println(name)
    println("    weights: $(wmg.w)")
    println("    ne: $(ne(wmg))")
    println("    nv: $(nv(wmg))")
end

end
