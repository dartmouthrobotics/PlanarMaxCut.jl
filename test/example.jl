#=
 * File: example.jl
 * Project: test
 * File Created: Sunday, 17th November 2019 11:27:51 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 18th November 2019 3:25:08 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

import PyPlot
using LightGraphs
using MetricTools
using MetricTools.MetricGraphs

if false
    include("../src/PlanarMaxCut.jl")
    using .PlanarMaxCut
else
    using PlanarMaxCut
end

function test(G::MetricGraph; lookahead=2::Integer, verbose=true::Bool)
    (algcut, algval, iters), t, bytes, gctime, memallocs = 
            @timed localsearch(G; initial=Set{Integer}([2, 5, 8]), lookahead=lookahead, verbose=verbose)
        ALG::String = "ALG$lookahead"
        println("    $ALG = $algval in $t secs")
    cutplot(G, cut2edges(G, algcut); name=ALG) 
end

function example2()
    G::MetricGraph = MetricGraph(2)
    e = 0.0001
    points::Array{Point} = [
        [1.1, 1.5],
        [1.2, 1.5],
        [1.3, 1.5],
        [1.4, 1.5],
        [1.5, 1.5],
        [1.6, 1.5],
        [1.7, 1.5],
        [1.8, 1.5],
        [1.9, 1.5],
    ]
    add!(G, points)
    
    edges::Dict{Edge, Float64} = Dict{Edge, Float64}(
        Edge(1, 2) => 1.,
        Edge(2, 3) => 1.,
        Edge(3, 4) => 1.,
        Edge(4, 5) => 1.,
        Edge(5, 6) => 1.,
        Edge(6, 7) => 1.,
        Edge(7, 8) => 1.,
        Edge(8, 9) => 1.,
    )
    add!(G, collect(keys(edges)))
    
    for (edge, w) in edges
        set_weight!(G, edge, w)
    end

    for edge in get_edges(G)
        d = distance(G, edge)
        println("d($edge) = $d")
    end
    test(G; lookahead=1)
    test(G; lookahead=2)
    test(G; lookahead=3)
end

example2()
