#=
 * File: graphgen.jl
 * Project: test
 * File Created: Wednesday, 23rd October 2019 2:32:54 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 18th November 2019 5:36:57 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using Test, LightGraphs
import PyPlot
using MetricTools, MetricTools.MetricGraphs

if false
    include("../src/PlanarMaxCut.jl")
    using
        .PlanarMaxCut,
        .PlanarMaxCut.PlanarGraphGenerator
else
    using
        PlanarMaxCut,
        PlanarMaxCut.PlanarGraphGenerator
end

function test_graphgeng(; n::Integer=10, name::String="graph1")
    G::MetricGraph = gengraphg(n)
    cutplot(G, Edge[]; name=name)
end

function test_graphgen(; n::Integer=10, name::String="graph1")
    G::MetricGraph, G_D::WeightedMultigraph, dualmap::DualMap = 
        gengraph(; n=n)
    cutplot(G, Edge[]; name=name)
end

# test_graphgeng(; n=10, name="graph1")
# test_graphgeng(; n=10, name="graph2")
# test_graphgeng(; n=10, name="graph3")

# test_graphgen(; n=10, name="graph4")
# test_graphgen(; n=10, name="graph5")
test_graphgen(; n=10, name="graph6")