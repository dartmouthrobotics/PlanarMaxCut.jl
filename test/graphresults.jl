#=
 * File: graphresults.jl
 * Project: test
 * File Created: Wednesday, 13th November 2019 2:11:31 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 18th November 2019 5:41:05 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#


import PyPlot, JSON2

if false
    include("../src/PlanarMaxCut.jl")
    using .PlanarMaxCut
else
    using PlanarMaxCut
end

function graphiters(;
        indir::String="data/input/",
        outdir::String="data/output/images/",
        filename::String="iters1")

    PyPlot.rc("font", family="serif", size=18)
    data::Array{Tuple{Integer, Float64}} = 
        JSON2.read(read("$indir$filename.json", String), Array{Tuple{Integer, Float64}})
    x, y = [e[1] for e in data], [e[2] for e in data]
    
    PyPlot.close("all")
    PyPlot.plot(x, y)
    PyPlot.xlabel("n")
    PyPlot.ylabel("Avg. iterations")
    println("$outdir$filename.png")
    PyPlot.tight_layout()
    PyPlot.savefig("$outdir$filename.png")
end

function graphres(;
        outdir::String="data/output/results/",
        series::String="OPT-ALG1-ALG2",
        filename::String="euclidian",
        n::Integer=100)
    
    dir::String = "$outdir$series/$n/"
    PyPlot.rc("font", family="serif", size=18)
    results::Array{ExperimentResult} = load(; dir=dir)
    graphresults(results; filename=filename)
end

graphres(; series="OPT-ALG1-ALG2-RW", filename="random")
graphres(; series="OPT-ALG1-ALG2", filename="euclidian")
# graphiters(; filename="iters2")
