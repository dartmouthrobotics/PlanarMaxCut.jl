#=
 * File: localsearch.jl
 * Project: test
 * File Created: Thursday, 31st October 2019 5:48:18 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 17th November 2019 11:19:20 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using MetricTools, MetricTools.MetricGraphs
if false
    include("../src/PlanarMaxCut.jl")
    using .PlanarMaxCut, .PlanarMaxCut.LocalSearch
else
    using PlanarMaxCut, PlanarMaxCut.LocalSearch
end


function test_flippers()
    
    cut::Set{Integer} = Set([1, 2, 3])
    flippers::Array{Integer} = [1, 3]
    println("cut: $cut")
    println("flippers: $flippers")
    
    LocalSearch.flip(cut, flippers)
    println("cut after flip: $cut")
    LocalSearch.flip(cut, flippers)
    println("cut after flip: $cut")
end

function test_localsearch1()
    G::MetricGraph = MetricGraph(2)

    ps::Array{Point} = [
        [0, 0],
        [0, 1],
        [1, 1],
        [1, 0]
    ]
    
    add!(G, ps)
    
    add!(G, (ps[1], ps[2]))
    add!(G, (ps[2], ps[3]))
    add!(G, (ps[3], ps[4]))
    add!(G, (ps[4], ps[1]))

    maxcut::Set{Integer}, maxval::Float64, iters::Integer =
        localsearch(G; lookahead=2, initial=Set([1, 2]))
    
    println("maxcut: $maxcut, maxval: $maxval")
end

"""
Tests the number of iterations of local search
"""
function test_localsearch_iters(;  lookahead=1::Integer)
    numrounds::Integer = 10
    for n in 1:100

        totali::Integer = 0
        for round in 1:numrounds
            G::MetricGraph, G_D::WeightedMultigraph, dualmap::DualMap =
                gengraph(; n=n)

            maxcut::Set{Integer}, maxval::Float64, i::Integer =
                localsearch(G; lookahead=lookahead, verbose=false)
            totali += i
        end

        println("[$n, $(totali / numrounds)],")
    end
end

test_localsearch_iters()

# test_flippers()
# test_localsearch1()
