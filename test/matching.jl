#=
 * File: matching.jl
 * Project: test
 * File Created: Thursday, 31st October 2019 6:02:52 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Tuesday, 12th November 2019 9:41:50 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using LightGraphs
using Test
if false
    include("../src/PlanarMaxCut.jl")
    using .PlanarMaxCut.PerfectMatching
else
    using PlanarMaxCut.PerfectMatching
end

function gw1()
    w = Dict(Edge(1,2)=> 500)
    g = Graph(2)
    add_edge!(g,1,2)
    return g, w
end

function testgw1(match)
    @test match.mate[1] == 2
end

function testgw1max(match)
    @test match.mate[1] == 2
end

function gw2()
    w = Dict(
        Edge(1,2)=>500,
        Edge(1,3)=>600,
        Edge(2,3)=>700,
        Edge(3,4)=>100,
        Edge(2,4)=>1000)

    g = complete_graph(4)
    return g, w
end

function testgw2(match)
    @test match.mate[1] == 2
    @test match.mate[2] == 1
    @test match.mate[3] == 4
    @test match.mate[4] == 3
    println(match.weight)
    @test match.weight ≈ 600
end

function testgw2max(match)
    @test match.mate[1] == 3
    @test match.mate[2] == 4
    @test match.mate[3] == 1
    @test match.mate[4] == 2
    println(match.weight)
    @test match.weight ≈ 1600
end

function gw3()
    w = Dict(
        Edge(1, 2) => 500,
        Edge(1, 3) => 400,
        Edge(2, 3) => 300,
        Edge(3, 4) => 1000,
        Edge(2, 4) => 1000)

    g = complete_graph(4)
    return g, w
end

function testgw3(match)
    @test match.mate[1] == 3
    @test match.mate[2] == 4
    @test match.mate[3] == 1
    @test match.mate[4] == 2
    println(match.weight)
    @test match.weight ≈ 1400
end

function gw4()
    g = complete_bipartite_graph(2,2)
    w = Dict{Edge,Float64}()
    w[Edge(1,3)] = -10
    w[Edge(1,4)] = -0.5
    w[Edge(2,3)] = -11
    w[Edge(2,4)] = -1
    return g, w
end

function testgw4(match)
    @test match.mate[1] == 4
    @test match.mate[4] == 1
    @test match.mate[2] == 3
    @test match.mate[3] == 2
    println(match.weight)
    @test match.weight ≈ -11.5
end

function gw5()
    g = complete_graph(4)
    w = Dict{Edge,Float64}()
    w[Edge(1,3)] = 10
    w[Edge(1,4)] = 0.5
    w[Edge(2,3)] = 11
    w[Edge(2,4)] = 2
    w[Edge(1,2)] = 100

    return g, w
end

function testgw5(match)
    @test match.mate[1] == 4
    @test match.mate[4] == 1
    @test match.mate[2] == 3
    @test match.mate[3] == 2
    println(match.weight)
    @test match.weight ≈ 11.5
end

function runtests(tests, func)
    for (i, test) in enumerate(tests)
        println("$i: ")
        gw, testgw = test
        g, w = gw()
        if i == 5
            match = func(g, w, 50)
        else
            match = func(g, w)
        end
        
        testgw(match)
        println()
    end
end

@testset "minwpm" begin
    tests = [
        # (gw1, testgw1),
        # (gw2, testgw2),
        # (gw3, testgw3),
        # (gw4, testgw4),
        # (gw5, testgw5)
    ]
    
    runtests(tests, minwpm)
end

@testset "maxwpm" begin
    tests = [
        (gw1, testgw1max),
        (gw2, testgw2max)
    ]
    runtests(tests, maxwpm)
end
