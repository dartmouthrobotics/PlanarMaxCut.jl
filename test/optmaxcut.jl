#=
 * File: optmaxcut.jl
 * Project: test
 * File Created: Tuesday, 12th November 2019 4:41:14 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 17th November 2019 11:38:56 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#
import PyPlot
using LightGraphs
using MetricTools, MetricTools.MetricGraphs

if false
    include("../src/PlanarMaxCut.jl")
    using .PlanarMaxCut
else
    using PlanarMaxCut
end

"""
Tests Optimal Max Cut on a simple case
"""
function test_simple()
    points::Array{Point} = [
        [1.2, 1.2],
        [1.35, 1.8],
        [1.5, 1.2],
        [1.65, 1.8],
        [1.8, 1.2],
    ]

    points = [
        [1.758306573168774,1.8858361502836272],
        [1.1956115288577207,1.233027194168011],
        [1.1295548805042797,1.6508562365336372],
        [1.6803887069475483,1.4771447123914343],
        [1.7876115221884847,1.3786915879637343],
        [1.0648312171309704,1.570918792429559],
        [1.824073974348531,1.3753696811266154]]
    
    # G::MetricGraph, G_D::WeightedMultigraph, dualmap::DualMap = 
    #     gengraph(points; randweights=false, verbose=true)

    G::MetricGraph, G_D::WeightedMultigraph, dualmap::DualMap = 
        gengraph(; n=10, randweights=false)
    runexp(G, G_D, dualmap; verbose=true)
end

test_simple()
