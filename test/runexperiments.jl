#=
 * File: runexperiments.jl
 * Project: test
 * File Created: Tuesday, 12th November 2019 9:50:37 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 18th November 2019 5:42:14 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

import PyPlot
using LightGraphs, Combinatorics

using MetricTools, MetricTools.MetricGraphs

if false
    include("../src/PlanarMaxCut.jl")
    using .PlanarMaxCut
else
    using PlanarMaxCut
end

function runexpswithdels()
    points::Array{Point} = 
        [[1.758306573168774,1.8858361502836272],
        [1.1956115288577207,1.233027194168011],
        [1.1295548805042797,1.6508562365336372],
        [1.6803887069475483,1.4771447123914343],
        [1.7876115221884847,1.3786915879637343],
        [1.0648312171309704,1.570918792429559],
        [1.824073974348531,1.3753696811266154]]
    rundelexps(points; verbose=true)
end

function runrandexps(;
        lookaheads::Array{Integer}=Integer[1, 2],
        n::Integer=50,
        exps::Integer=10,
        randweights::Bool=false,
        verbose::Bool=false)
    for exp in 1:exps
        runrandexp(;
            n=n, lookaheads=lookaheads,
            randweights=randweights, verbose=verbose)
    end
end

"""
Forces a call to the julia JIT compiler to compile optmaxcut and localsearch
"""
function warmup()
    G::MetricGraph, G_D::WeightedMultigraph, dualgraph::Dict{Tuple{Edge, Float64}, Edge} = 
        gengraph(; n=4)
    optmaxcut(G, G_D, dualgraph)
    localsearch(G)
end

warmup()
runrandexps(; lookaheads=Integer[1, 2], n=10, exps=1, verbose=true, randweights=false)
# runexpswithdels()
