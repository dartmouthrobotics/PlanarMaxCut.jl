#=
 * File: runtests.jl
 * Project: test
 * File Created: Wednesday, 23rd October 2019 2:32:10 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Thursday, 31st October 2019 7:01:22 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#


using MetricTools
using Test

function testtuple()
    tup::Tuple{Vararg{Integer}} = (1, 2, 3, 4)
end

testtuple()
