#=
 * File: weights.jl
 * Project: test
 * File Created: Wednesday, 13th November 2019 4:39:48 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 13th November 2019 5:09:15 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using PlanarMaxCut
using MetricTools, MetricTools.MetricGraphs

function randomize_weights()
    G::MetricGraph, G_D::WeightedMultigraph = gengraph(;
        n=5, randweights=true, verbose=false)
    
    println(G_D)
end

randomize_weights()
