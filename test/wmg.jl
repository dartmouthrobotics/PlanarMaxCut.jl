#=
 * File: wmg.jl
 * Project: test
 * File Created: Saturday, 9th November 2019 6:03:46 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Tuesday, 12th November 2019 9:51:58 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using LightGraphs, Test

if false
    include("../src/PlanarMaxCut.jl")
    using .PlanarMaxCut
else
    using PlanarMaxCut
end


function testwmg()
    wmg::WeightedMultigraph = WeightedMultigraph()
    for i in 1:5 add_vertex!(wmg) end
    add_edge!(wmg, 1, 2, 1.)
    add_edge!(wmg, 2, 1, 2.)
    add_edge!(wmg, 1, 3, 1.)

    @test degree(wmg, 1) == 3
    @test ne(wmg) == 3
    @test nv(wmg) == 5
    println("done")
end

@testset "WeightedMultigraphs test" begin
    testwmg()
end
